inherit kernel

require recipes-kernel/linux/linux-dtb.inc

PR = "r1"

DESCRIPTION = "Linux kernel for AT91SAM9X5 chips"
KERNEL_IMAGETYPE = "zImage"

KCONFIG_MODE="--alldefconfig"

LICENSE = "GPLv3"

COMPATIBLE_MACHINE = "(netsam9x25|sama5d3_xplained_light)"

DEFAULT_PREFERENCE = "-1"
DEFAULT_PREFERENCE_at91sam9x5ek = "99"

FILESEXTRAPATHS_prepend := "${THISDIR}/linux-at91sam9x5ek-${MACHINE}:"

SRC_URI = " \
           ${KERNELORG_MIRROR}/linux/kernel/v4.x/linux-${PV}.tar.xz \
           file://0001-activate-watchdog-for-sama5d3_xplained-board.patch \
           file://defconfig \
          "

SRC_URI[md5sum] = "4edd7cca8a61a48034a9087fd35610f4"
SRC_URI[sha256sum] = "e314ec09af537c3d17afc34706fc2190c69bc04bcee69fcc448cd0ebfdb164a3"

LIC_FILES_CHKSUM = "file://COPYING;md5=d7810fab7487fb0aad327b76f1be7cd7"

S = "${WORKDIR}/linux-${PV}"

