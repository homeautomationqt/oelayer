SUMMARY = "gmrender-resurrect: UPnP/DLNA renderer"
HOMEPAGE = "https://github.com/hzeller/gmrender-resurrect"
LICENSE = "BSD"

LIC_FILES_CHKSUM = "file://LICENSE;md5=b3190d5244e08e78e4c8ee78544f4863"

inherit autotools pkgconfig

SRC_URI = "git://git.code.sf.net/p/pupnp/code;branch=branch-1.6.x \
"

# latest / master HEAD
SRCREV = "51a01cdba12ff1d236e189d99c745a8133e8bd87"
PV = "1.6.19+git${SRCPV}"

# for license file
SRC_URI[md5sum] = "152dd5824e42f9435f7edfd049f384d5"
SRC_URI[sha256sum] = "68eaa7da71e64c4aeeb097d5095e6f7f6f4554031b15324abc4b3dfc685f9039"

S = "${WORKDIR}/git"
B = "${WORKDIR}/git"

