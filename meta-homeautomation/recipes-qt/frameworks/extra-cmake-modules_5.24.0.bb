LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://COPYING-CMAKE-SCRIPTS;md5=54c7042be62e169199200bc6477f04d1"

DEPENDS = "qttools-native"

inherit cmake_qt5
require frameworks-common.inc

FILES_${PN} += "\
                /usr/man/* \
  /usr/share/ECM/cmake/ECMConfig.cmake \
  /usr/share/ECM/cmake/ECMConfigVersion.cmake \
  /usr/share/ECM/find-modules/FindLibUSB1.cmake \
  /usr/share/ECM/find-modules/FindX11_XCB.cmake \
  /usr/share/ECM/find-modules/FindBlueZ.cmake \
  /usr/share/ECM/find-modules/FindXCB.cmake \
  /usr/share/ECM/find-modules/FindDocBookXML.cmake \
  /usr/share/ECM/find-modules/FindDocBookXSL.cmake \
  /usr/share/ECM/find-modules/FindQtWaylandScanner.cmake \
  /usr/share/ECM/find-modules/FindWaylandScanner.cmake \
  /usr/share/ECM/find-modules/FindPoppler.cmake \
  /usr/share/ECM/kde-modules/KDECMakeSettings.cmake \
  /usr/share/ECM/kde-modules/KDEInstallDirs.cmake \
  /usr/share/ECM/kde-modules/KDECompilerSettings.cmake \
  /usr/share/ECM/modules/ECMGenerateHeaders.cmake \
  /usr/share/ECM/modules/ECMDBusAddActivationService.cmake \
  /usr/share/ECM/modules/ECMAddTests.cmake \
  /usr/share/ECM/modules/ECMUseFindModules.cmake \
  /usr/share/ECM/modules/ECMVersionHeader.h.in \
  /usr/share/ECM/modules/ECMSetupVersion.cmake \
  /usr/share/ECM/modules/ECMMarkNonGuiExecutable.cmake \
  /usr/share/ECM/modules/CMakePackageConfigHelpers.cmake \
  /usr/share/ECM/modules/ECMInstallIcons.cmake \
  /usr/share/ECM/modules/ECMMarkAsTest.cmake \
  /usr/share/ECM/modules/ECMOptionalAddSubdirectory.cmake \
  /usr/share/ECM/kde-modules/KDEFrameworkCompilerSettings.cmake \
  /usr/share/ECM/find-modules/FindEGL.cmake \
  /usr/share/ECM/find-modules/FindOpenEXR.cmake \
  /usr/share/ECM/find-modules/ECMFindModuleHelpersStub.cmake \
  /usr/share/ECM/find-modules/FindWayland.cmake \
  /usr/share/ECM/find-modules/FindSharedMimeInfo.cmake \
  /usr/share/ECM/find-modules/FindKF5.cmake \
  /usr/share/ECM/find-modules/FindLibGit2.cmake \
  /usr/share/ECM/modules/ECMGeneratePriFile.cmake \
  /usr/share/ECM/modules/ECMPackageConfigHelpers.cmake \
  /usr/share/ECM/modules/ECMFindModuleHelpers.cmake \
  /usr/share/ECM/modules/ECMCreateQmFromPoFiles.cmake \
  /usr/share/ECM/modules/ECMQmLoader.cpp.in \
  /usr/share/ECM/modules/ECMPoQmTools.cmake \
  /usr/share/ECM/modules/ECMCoverageOption.cmake \
  /usr/share/ECM/modules/ECMGeneratePkgConfigFile.cmake \
  /usr/share/ECM/modules/ECMEnableSanitizers.cmake \
  /usr/share/ECM/modules/ECMQueryQmake.cmake \
  /usr/share/ECM/toolchain \
  /usr/share/ECM/modules/ECMAddAppIcon.cmake \
  /usr/share/ECM/modules/ECMUninstallTarget.cmake \
  /usr/share/ECM/modules/ECMQtDeclareLoggingCategory.cpp.in \
  /usr/share/ECM/modules/ECMQtDeclareLoggingCategory.h.in \
  /usr/share/ECM/modules/ECMQtDeclareLoggingCategory.cmake \
  /usr/share/ECM/modules/ecm_uninstall.cmake.in \
  /usr/share/ECM/find-modules/FindPng2Ico.cmake \
  /usr/share/ECM/toolchain/Android.cmake \
  /usr/share/ECM/toolchain/deployment-file.json.in \
  /usr/share/ECM/toolchain/specifydependencies.cmake \
  /usr/share/ECM/kde-modules/KDEPackageAppTemplates.cmake \
               "

SRC_URI[md5sum] = "cd3b0c844234ad29cfdba89d63ccb2ae"
SRC_URI[sha256sum] = "8a4fd5eac37d4c6a4998c48716efe2392c6f7ec9a124aab4c8fc26516815a106"

