DESCRIPTION = "UPnP stuff"

LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e6a600fd5e1d9cbde2d983680233ad02"

PR = "r1"

DEPENDS = "extra-cmake-modules qtbase kdsoap qtwebsockets qtdeclarative"

PACKAGE_DEBUG_SPLIT_STYLE = "debug-without-src"

inherit cmake_qt5

SRCREV = "ebe92158181f4802f862719aebdeec8cb5a2ccc8"

SRC_URI = "git://gitlab.com/homeautomationqt/upnp-player-qt.git;protocol=https;branch=master"

SRC_URI[md5sum] = "00d54838a6026b0d3c7171cd32dfe193"
SRC_URI[sha256sum] = "0a0b3e3292dd64b431f26dfd10970b9898b887f8c79705c6c2946f307e131bf0"

S = "${WORKDIR}/git"

FILES_${PN}-dev = " \
  ${libdir}/cmake/UPNPQT/UPNPQTTargets-relwithdebinfo.cmake \
  ${libdir}/cmake/UPNPQT/UPNPQTConfigVersion.cmake \
  ${libdir}/cmake/UPNPQT/UPNPQTConfig.cmake \
  ${libdir}/cmake/UPNPQT/UPNPQTTargets.cmake \
  ${libdir}/cmake/UPNPQT/UPNPQTTargets-noconfig.cmake \
  ${libdir}/libupnpQt.so \
  ${libdir}/libupnpQtBase.so \
  ${libdir}/libupnpQtWebSocket.so \
"

