DESCRIPTION = "KDSoap"

LICENSE = "AGPLv3"
LIC_FILES_CHKSUM = "file://LICENSE.AGPL3-modified.txt;md5=262a7c33566f3d0928fb48e245c79afc"

PR = "r1"

DEPENDS = "extra-cmake-modules qtbase"

#PACKAGE_DEBUG_SPLIT_STYLE = "debug-without-src"

inherit cmake_qt5

EXTRA_OECMAKE += " \
    -DKDSoap_TESTS=false \
"

SRC_URI = " \
    git://github.com/KDAB/KDSoap.git;protocol=https;branch=master \
    file://0001-deactivate-the-export-of-kdwsdl2cpp-target.patch \
    file://0002-deactivate-build-of-examples-due-to-cross-compilatio.patch \
"

SRCREV = "8f6f1fe6926210a7302cdcc5a051e3adfc1e9978"

SRC_URI[md5sum] = "00d54838a6026b0d3c7171cd32dfe193"
SRC_URI[sha256sum] = "0a0b3e3292dd64b431f26dfd10970b9898b887f8c79705c6c2946f307e131bf0"

S = "${WORKDIR}/git"

FILES_${PN} = "${libdir}/*${SOLIBS}"

FILES_${PN}-dev += " \
  ${bindir}/kdwsdl2cpp \
  ${datadir}/mkspecs \
  ${datadir}/mkspecs/features \
  ${datadir}/mkspecs/features/kdsoap.prf \
  ${libdir}/cmake \
  ${libdir}/cmake/KDSoap \
  ${libdir}/cmake/KDSoap/KDSoapConfig.cmake \
  ${libdir}/cmake/KDSoap/KDSoapConfigVersion.cmake \
  ${libdir}/cmake/KDSoap/KDSoapMacros.cmake \
  ${libdir}/cmake/KDSoap/KDSoapTargets.cmake \
  ${libdir}/cmake/KDSoap/KDSoapTargets-debug.cmake \
"

