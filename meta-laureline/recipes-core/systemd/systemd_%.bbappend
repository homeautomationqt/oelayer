PACKAGECONFIG_append := " resolved networkd"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
        file://80-dhcp.network \
        file://0001-disable-assert.patch \
"

FILES_${PN}_prepend := "${libdir}/systemd/network"

do_install_append() {
        cp ${WORKDIR}/80-dhcp.network ${D}${libdir}/systemd/network
}

