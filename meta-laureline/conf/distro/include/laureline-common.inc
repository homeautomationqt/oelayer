DISTRO_FEATURES = "largefile ${DISTRO_FEATURES_LIBC} multiarch systemd pam ipv6 nfs smbfs acl xattr usbgadget usbhost selinux alsa pulseaudio"

require conf/distro/poky.conf

VIRTUAL-RUNTIME_dev_manager   = "udev"
VIRTUAL-RUNTIME_init_manager  = "systemd"
VIRTUAL-RUNTIME_initscripts   = ""
VIRTUAL-RUNTIME_login_manager = "shadow"
VIRTUAL-RUNTIME_base-utils ?= "coreutils"

PREFERRED_PROVIDER_ffmpeg = "ffmpeg"
PREFERRED_VERSION_libav ?= "9.16"

DISTRO_NAME = "Laureline"
DISTRO_VERSION = "0.3"
DISTRO_CODENAME = "next"

DISTRO_FEATURES_BACKFILL_CONSIDERED = "sysvinit"

DISTROOVERRIDES = "poky"

